#include "structure.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


Pix** initImgTab(unsigned long x, unsigned long y)
{

    unsigned long i=0;
    Pix** array;
    array = NULL;
    array = malloc(x * sizeof(*array));
    for(i=0;i<x;i++){
	array[i]=(Pix *) malloc(y*sizeof(Pix));}
    
    return array;
}



int adapt(int n, int x, int y){
	if (n < 0) {
		return 0;
	} else if (n > 255){
		return 255;
	} else {
		return round(n);
	}
}

/*
 convolution(int (*)[5] image, int (*)[3] filtre){

	int** res[5][5];
	int x = 5;
	int y = 5;

	for (int i = 2; i < x; ++i){    // parcours de l'image

		for (int j = 2; j < y; ++j){

			int newVal = 0;

			for (int k = i-2; x < 3; ++x) {		// parcours des pixels alentours

				for (int l = j-2; y < 3; ++y) {
					
					newVal = newVal + (img[k][l] * filtre[k + 1][l + 1]);

				}
			}

			adapt(newVal,0,255);

			**res[i][j] = newVal;

			newVal = 0;
		}
	}
	return **res;
}

int tab[5][5] = {{100, 100, 100, 100, 100 }, {100, 100, 100, 100, 100 }, {100, 100, 150, 100, 100 },{100, 100, 100, 100, 100 },{100, 100, 100, 100, 100 }};

int filtre[3][3] = {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}};

int** a = convolution(tab,filtre);

*/

Image convolution(Image img, int** filtre){

	int x = img.x;
	int y = img.y;

	Pix** res = initImgTab(x,y); // initialisation d'une matrice de pixels de taille x*y

	for (int i = 2; i < x; ++i){    // parcours de l'image

		for (int j = 2; j < y; ++j){

			int newR = 0;
			int newG = 0;
			int newB = 0;

			for (int k = i-2; x < 3; ++x) {		// parcours des pixels alentours

				for (int l = j-2; y < 3; ++y) {
					
					newR = newR + (img.array[k][l].r * filtre[k + 1][l + 1]);
					newG = newG + (img.array[k][l].g * filtre[k + 1][l + 1]);
					newB = newB + (img.array[k][l].b * filtre[k + 1][l + 1]);

				}
			}

			adapt(newR,0,255); // réajustement des valeurs pour qu'elles soient comprises entre 0 et 255
			adapt(newG,0,255);
			adapt(newB,0,255);

			res[i][j].r = newR; // on update les valeurs du pixel
			res[i][j].g = newG;
			res[i][j].b = newB;

			newR = 0; // remise à 0 des nouveaux pixels
			newG = 0;
			newB = 0;
		}
	}

	img.array = res;

	return img;
}

int main (){
	return 1;
}