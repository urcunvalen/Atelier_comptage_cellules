function [m] = gauss(x,y,sigma)
  pi = 3.1415926535897932384626433832795;
  m = (1 / (2 * pi * sigma)) * exp(-(((x**2)+(y**2))/(2 * (sigma ** 2))));
endfunction