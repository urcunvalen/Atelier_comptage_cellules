
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "structure.h" // inclusion du header contenant les structures
#include "Main.c"
//Image 



int stdseuil(Image mat)
{
	int res, i, j;
	float mean, stddev,ngris,dev;
	mean = 0;
	stddev = 0;
	dev = 0;
	for (i = 0 ; i <= mat.x - 1; ++i)
	{
		for (j = 0; j <= mat.y - 1; ++j)
		{
			ngris = (float)(mat.array[i][j].r + mat.array[i][j].g + mat.array[i][j].b) / 3 ;
			mean += ngris;
		}		
	}

	mean = mean/mat.n;
	for (i = 0 ; i <= mat.x - 1 ; ++i)
	{
		for (j = 0; j <= mat.y - 1; ++j)
		{
			ngris = (float)(mat.array[i][j].r + mat.array[i][j].g + mat.array[i][j].b) / 3 ;
			dev = pow((ngris - mean),2);
			stddev += dev;
		}		
	}
	stddev = stddev/mat.n;
	res = trunc(sqrt(stddev)+ mean);//peut etre mettre un coef de O.2 a lecart type
	return res;
}

Pix setcolor(Pix pix, int n)
{
	pix.r = n;
	pix.g = n;
	pix.b = n;
	return pix;
}

Pix binar(Pix pix, int seuil)
{
	int ngris;
	ngris = (pix.r + pix.g + pix.b) / 3 ;
	if (ngris >= seuil){
		pix = setcolor(pix,255);
	} 
	else {
		pix = setcolor(pix,0);
	}
	return pix;
}


Image fseuil (Image mat)
{
	Image imseuil; 
	int seuil, i, j;
	Pix k;
	seuil = stdseuil(mat);
	for (i = 0 ; i <= mat.x - 1; ++i)
	{
		for (j = 0; j <= mat.y - 1; ++j)
		{
			k = binar(mat.array[i][j],seuil);
			imseuil.array[i][j]= k;
		}		
	}
	return imseuil;
}


// int main(){
//     Image img;
//     img = readfile();
//     //img = greyImg(img);
//     // histo = makeHistogram(img);
//     // img = recadrage(img);
//     // img = binarise(img,histo,getOstu(img,histo));
//     img = fseuil(img);
//      printfile(img);
//     desalloc(img);
//     return 0;
// }
