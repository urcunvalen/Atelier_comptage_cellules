#include <stdio.h>
#include <stdlib.h>
#include "structure.h" // inclusion du header contenant les structures
#include "array.h"


//Image 
Pix** initImgTab(unsigned long x, unsigned long y)
{

    unsigned long i=0;
    Pix** array;
    array = NULL;
    array = malloc(x * sizeof(*array));
    for(i=0;i<x;i++){
	array[i]=(Pix *) malloc(y*sizeof(Pix));}
    
    return array;
}

void desalloc(Image img)
{
    unsigned long i=0;
    for (i = 0 ; i < img.x ; i++){
    	free(img.array[i]);
	
    }
    free(img.array);
}

Image readfile(){
    Image img;
    unsigned long  x=0,y=0,i=0,j=0,z;
    FILE* file = NULL;
    /* char name[100]; */
    /* printf("Quel fichier voulez vous lire?"); */
    /* scanf("%s",name); */
    file = fopen("test.ppm","r");
    if (file != NULL){
	char p3[200], comment[200];
	fgets(p3,200,file);
	fgets(comment,200,file);
	fscanf(file,"%d %d %d", &x, &y,&z);
	img.x = x;
	img.y = y;
	img.n = x * y;
	img.t = z;
	img.array = initImgTab(x,y);
    	int r,g,b;
	for(i=0;i<x;i++){
	    for(j=0;j<y;j++){
		if (fscanf(file,"%d %d %d",&r,&g,&b)!=EOF){;
		    img.array[i][j].r = r;
		    img.array[i][j].g = g;
		    img.array[i][j].b = b;
		    img.array[i][j].state = UNKNOWN;
		}
	    }
	}
	fclose(file); 
    }
    else {
	printf("Houston, we've had a problem");
    }
    return img;
}

void printfile(Image img){
    unsigned long i=0,j=0;
    /* char name[100]; */
    /* printf("Quel fichier voulez vous lire?"); */
    /* scanf("%s",name); */
    FILE* file = NULL;
    file = fopen("testEnd.ppm","w+");
    if (file != NULL){
	fprintf(file,"P3\n#Cette ligne est aussi inutile qu'Eliott\n%d %d\n%d\n",img.x,img.y,img.t);
	for(i=0;i<img.x;i++){
	    for(j=0;j<img.y;j++){
		fprintf(file,"%d\n%d\n%d\n",img.array[i][j].r,img.array[i][j].g,img.array[i][j].b);}
	}
	fclose(file); 
    }
    else {
	printf("Houston, we've had a problem");
    }
}





int** initIntTab(unsigned long x, unsigned long y)
{

    unsigned long i=0;
    int** array;
    array = NULL;
    array = malloc(x * sizeof(*array));
    for(i=0;i<x;i++){
	array[i]=(int *) malloc(y*sizeof(Pix));}
    
    return array;
}

int** gaussSigmaUn(int** matrix){
    matrix[0][0]=matrix[4][0]=matrix[0][4]=matrix[4][4] = 1;
    matrix[0][1]=matrix[0][3]=matrix[1][0]=matrix[1][4]=matrix[3][0]=matrix[3][4]=matrix[4][1]=matrix[4][3] = 4;
    matrix[0][2]=matrix[2][0]=matrix[2][4]=matrix[4][2] = 7;
    matrix[1][1]=matrix[1][3]=matrix[3][1]=matrix[3][3] = 16;
    matrix[1][2]=matrix[2][1]=matrix[3][2]=matrix[2][3] = 26;
    matrix[2][2]=41;
    return matrix;
}



void initMatrix(){
    int** matrix = NULL;
    int i=0;
    matrix = initIntTab(5,5);
    matrix = gaussSigmaUn(matrix);
    for(i=0;i<5;i++){
	printf("%d %d %d %d %d\n",matrix[i][0],matrix[i][1],matrix[i][2],matrix[i][3]),matrix[i][4];}
}






    
int main(){
    /* Image img; */
    /* img = readfile(); */
    
    /* printfile(img); */
    /* desalloc(img); */
    initMatrix();
    return 0;
}
