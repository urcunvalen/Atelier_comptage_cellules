#ifndef __DEF_STRUCTURES_ARRAY_ // Si la constante n'a pas été définie le fichier n'a jamais été inclus

#define __DEF_STRUCTURES_ARRAY_ // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "structure.h" // inclusion du header contenant les structures
#include "array.h"




Pix** initImgTab(unsigned long x, unsigned long y)
{

    unsigned long i=0;
    Pix** array;
    array = NULL;
    array = malloc(x * sizeof(Pix*));
    for(i=0;i<x;i++){
	array[i]=malloc(y*sizeof(Pix));}
    
    return array;
}


void desalloc(Image img)
{
    unsigned long i=0;
    for (i = 0 ; i < img.x ; i++){
    	free(img.array[i]);
	
    }
    free(img.array);
}

#endif
