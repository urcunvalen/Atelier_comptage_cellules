#ifndef __DEF_STRUCTURES_MAIN_ // Si la constante n'a pas été définie le fichier n'a jamais été inclus

#define __DEF_STRUCTURES_MAIN_ // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "structure.h" // inclusion du header contenant les structures
#include "array.h"
#include "array.c"
#include "convo.c"
#include "convo.h"
#include "seuillage.c"


//Image 




Image readfile(){
    Image img;
    unsigned long  x=0,y=0,i=0,j=0,z;
    FILE* file = NULL;
    /* char name[100]; */
    /* printf("Quel fichier voulez vous lire?"); */
    /* scanf("%s",name); */
    file = fopen("testC.ppm","r");
    if (file != NULL){
	char p3[200], comment[200];
	fgets(p3,200,file);
	fgets(comment,200,file);
	fscanf(file,"%d %d %d", &x, &y,&z);
	img.x = x;
	img.y = y;
	img.n = x * y;
	img.t = z;
	img.array = initImgTab(x,y);
    	int r,g,b;
	for(i=0;i<x;i++){
	    for(j=0;j<y;j++){
		if (fscanf(file,"%d %d %d",&r,&g,&b)!=EOF){;
		    img.array[i][j].r = r;
		    img.array[i][j].g = g;
		    img.array[i][j].b = b;
		    img.array[i][j].state = UNKNOWN;
		}
	    }
	}
	fclose(file); 
    }
    else {
	printf("Houston, we've had a problem");
    }
    return img;
}

Image greyImg(Image img){
    unsigned long i=0,j=0;
    int gris;
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    gris = ceil((img.array[i][j].r * 0.3) + (img.array[i][j].g * 0.59) + (img.array[i][j].b * 0.11));
	    /* printf("%f\n%d\n\n-------\n",((img.array[i][j].r * 0.3) + (img.array[i][j].g * 0.59) + (img.array[i][j].b * 0.11)),(int)ceil((img.array[i][j].r * 0.3) + (img.array[i][j].g * 0.59) + (img.array[i][j].b * 0.11))); */
	    img.array[i][j].r = img.array[i][j].g = img.array[i][j].b = gris;
	    
	}
    }
    return img;
}
    



void printfile(Image img){
    unsigned long i=0,j=0;
    /* char name[100]; */
    /* printf("Quel fichier voulez vous lire?"); */
    /* scanf("%s",name); */
    printf("%d--------%d\n",img.x,img.y);
    FILE* file = NULL;
    file = fopen("testEnd.ppm","w");
    if (file != NULL){
	fprintf(file,"P3\n#Cette ligne est aussi inutile qu'Eliott\n%d %d\n%d\n",img.x,img.y,img.t);
	for(i=0;i<img.x;i++){
	    for(j=0;j<img.y;j++){
		fprintf(file,"%d\n%d\n%d\n",img.array[i][j].r,img.array[i][j].g,img.array[i][j].b);
		//printf("%d || %d || %d ---- %d | %d\n",img.array[i][j].r,img.array[i][j].g,img.array[i][j].b,i,j);
	    }
	}
	fclose(file); 
    }
    else {
	printf("Houston, we've had a problem");
    }
}

int * makeHistogram(Image img){
    static int histo[256] = {0};
    unsigned long i=0,j=0;
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    histo[img.array[i][j].r]++; 
	}
    }
    /* for(i=0;i<256;i++){ */
    /* 	printf("%d : %d\n------\n",i,histo[i]); */
    /* } */
    return histo;
}



Image recadrage(Image img){
    unsigned long i=0,j=0;
    double delta;
    int minI=255, maxI=0;
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    if (img.array[i][j].r > maxI){
		maxI = img.array[i][j].r;
	    }
	    if (img.array[i][j].r < minI){
		minI = img.array[i][j].r;
	    }
	}
    }
//  printf("max : %d | min : %d\n ---- \n",maxI,minI);
    delta = 255 / (maxI - minI);
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    img.array[i][j].r = img.array[i][j].g = img.array[i][j].b = ceil((img.array[i][j].r - minI)*delta); 
	}
    }
    return img;
}


int getMediane(Image img, int *histo){
    unsigned long i=0,j=0;
    int mediane;
    int position = ceil(img.n / 2);
    while (i<position){
	i += histo[j];
	j++;
    }
    mediane = j-1;
    return mediane;
}


//méthode Otsu, reference : Wikipédia
int getOstu(Image img, int *histo){
    int sumB=0,wB=0,wF=0,i=0,j=0,sum1 =0;
    int otsu;
    double maximum = 0.0,mF = 0.0,between=0.0;;
    for(i=0;i<256;i++){
	sum1 += i * histo[i];
    }
    for(j=0;j<256;j++){
	wB += histo[j];
	wF = img.n - wB;
	if (!(wB == 0 || wF == 0)){
	    sumB += ((j) * histo[j]);
	    mF = ((sum1 - sumB) / wF);
	    between = (wB) * (wF) * ((sumB / wB) - mF) * ((sumB / wB) - mF);
	    if (between >= maximum){
		otsu = j;
		maximum = between;
	    }
	}
    }
    return otsu;
}

Image binarise (Image img, int *histo,int mediane){
    unsigned long i=0,j=0;
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    (img.array[i][j].r > mediane) ? (img.array[i][j].r=img.array[i][j].g=img.array[i][j].b = 255) : (img.array[i][j].r=img.array[i][j].g=img.array[i][j].b = 0);
		
	}
    }
    return img;
}




int countA(Image img){
    static int histo[2] = {0};
    static int back[2] = {0};
    int i=0,j=0,color = 1;
    int fond;
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    histo[(img.array[i][j].r) % 254]++;
	    printf("%d %d : %d --- %d\n",i,j,img.array[i][j].r,img.array[i][j].r % 254);
	}
    }
    (histo[0] > histo[1]) ? (fond = 0) : (fond = 255);
    for(i=0;i<img.x;i++){
	for(j=0;j<img.y;j++){
	    if (img.array[i][j].r != fond){

		if (j!=0){
		    if(i!=0){
			back[]
		    }
		}

		
	    }

	    
	}
    }
}




int main(){
    Image img;
    int *histo;
    double sigma;
    int j=0,i=0;
    sigma = 2;
    img = readfile();
    img = convoGauss(img,sigma);
    img = greyImg(img);
    histo = makeHistogram(img);
    /* for(i=0;i<256;i++){ */
    /* 	printf("%d : %d \n",i,histo[i]); */
    /* } */
    /* img = Sobel(img); */
    img = binarise(img,histo,getOstu(img,histo));  
    printfile(img);
    j = countA(img);
    desalloc(img);
    return 0;
}

#endif

//sqrt(-(radius^2) / (2*log10(1 / 255)));
//radius 21
