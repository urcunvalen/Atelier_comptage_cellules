#ifndef __DEF_STRUCTURES_CONVO_ // Si la constante n'a pas été définie le fichier n'a jamais été inclus

#define __DEF_STRUCTURES_CONVO_ // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "structure.h" // inclusion du header contenant les structures
#include "array.h"
#include "array.c"

// gcc -std=c99 -lm convo.c


int adapt(double n){
//    max(0,min(round(n),255));
    if (n < 0) {
	return 0;
    } else if (n > 255){
	return 255;
    } else {
	return (round(n));
    }
}


double gaussianFilter(int x, int y, double sigma){
    double a = (x*x + y*y)/(2*sigma*sigma);
    double b = 2*3.14159265358979323*sigma*sigma;
    double c = (1/b) * (exp(-a));
    return (c);
}




Image convoGauss(Image img, double sigma){

    int x = img.x;
    int y = img.y;
    double newR = 0;
    double newG = 0;
    double newB = 0;
    Pix** res = initImgTab(x,y); // initialisation d'une matrice de pixels de taille x*y
    int trueX,trueY;
    for (int i = 0; i < x; i++){    // parcours de l'image

	for (int j = 0; j < y; j++){


	    if (((i > 1) && (i<(x-2))) || ((j>1) && (j<(y-2)))){

		//printf("%d || %d\n",i,j);
		for (int k = 0; k < 5; k++) {		// parcours des pixels alentours

		    for (int l = 0; l < 5; l++) {
			trueX = k-2;
			trueY = l-2;
			
			/* printf("i : %d || j : %d || k : %d || l : %d\n",i,j,k,l); */
			/* printf("X : %d || Y : %d\n -------------- \n",l,k); */
					
			newR = newR + gaussianFilter(trueY,trueX,sigma) * img.array[i][j].r;
			newG = newG + gaussianFilter(trueY,trueX,sigma) * img.array[i][j].g;
			newB = newB + gaussianFilter(trueY,trueX,sigma) * img.array[i][j].b;
		    }
		}

		newR = adapt(newR); // réajustement des valeurs pour qu'elles soient comprises entre 0 et 255
		newG = adapt(newG);
		newB = adapt(newB);

		res[i][j].color = 0;
		res[i][j].state = UNKNOWN;
		res[i][j].r = newR; // on update les valeurs du pixel
		res[i][j].g = newG;
		res[i][j].b = newB;

		newR = 0; // remise à 0 des nouveaux pixels
		newG = 0;
		newB = 0;
	    }else {
	
		res[i][j].r = img.array[i][j].r; // on update les valeurs du pixel
		res[i][j].g = img.array[i][j].g;
		res[i][j].b = img.array[i][j].b;
		//printf("-----------------\n%d || %d\n--------------\n%d | %d | %d\n",i,j,res[i][j].r,res[i][j].g,res[i][j].b);


	    }
	}
    }

    img.array = res;

    return img;
}


Image Sobel(Image img){

    int x = img.x;
    int y = img.y;
    Pix GX,GY,G;
    
    Pix** res = initImgTab(x,y); // initialisation d'une matrice de pixels de taille x*y
    int trueX,trueY;
    for (int i = 0; i < x; i++){    // parcours de l'image
	for (int j = 0; j < y; j++){
	    printf("%d || %d\n",i,j);
	    if (((i > 0) && (i<(x-1))) && ((j>0) && (j<(y-1)))){
		    GX.r = (img.array[i-1][j-1].r * -1) + (img.array[i+1][j+1].r) + (img.array[i+1][j-1].r) + (img.array[i-1][j+1].r * -1) + (img.array[i-1][j].r * -2) + (img.array[i+1][j].r * 2);
		    GX.g = (img.array[i-1][j-1].g * -1) + (img.array[i+1][j+1].g) + (img.array[i+1][j-1].g) + (img.array[i-1][j+1].g * -1) + (img.array[i-1][j].g * -2) + (img.array[i+1][j].g * 2);
		    GX.b = (img.array[i-1][j-1].b * -1) + (img.array[i+1][j+1].b) + (img.array[i+1][j-1].b) + (img.array[i-1][j+1].b * -1) + (img.array[i-1][j].b * -2) + (img.array[i+1][j].b * 2);
		    
		    GY.r = (img.array[i-1][j-1].r) + (img.array[i+1][j+1].r * -1) + (img.array[i+1][j-1].r) + (img.array[i-1][j+1].r * -1) + (img.array[i][j+1].r * -2) + (img.array[i][j-1].r * 2);
		    GY.g = (img.array[i-1][j-1].g) + (img.array[i+1][j+1].g * -1) + (img.array[i+1][j-1].g) + (img.array[i-1][j+1].g * -1) + (img.array[i][j+1].g * -2) + (img.array[i][j-1].g * 2);
		    GY.b = (img.array[i-1][j-1].b) + (img.array[i+1][j+1].b * -1) + (img.array[i+1][j-1].b) + (img.array[i-1][j+1].b * -1) + (img.array[i][j+1].b * -2) + (img.array[i][j-1].b * 2);
		    G.r = (GX.r * 0.5) + (GY.r * 0.5);
		    G.g = (GX.g * 0.5) + (GY.g * 0.5);
		    G.b = (GX.b * 0.5) + (GY.b * 0.5);
		    res[i][j].r = adapt(G.r); // on update les valeurs du pixel
		    res[i][j].g = adapt(G.g);
		    res[i][j].b = adapt(G.b);
		    
		}else {
		res[i][j].r = img.array[i][j].r; // on update les valeurs du pixel
		res[i][j].g = img.array[i][j].g;
		res[i][j].b = img.array[i][j].b;
		//printf("-----------------\n%d || %d\n--------------\n%d | %d | %d\n",i,j,res[i][j].r,res[i][j].g,res[i][j].b);


	    }
	}
    }

    img.array = res;

    return img;
}





#endif
