Compte le nombre de régions sur l'image.

Entrée : image (format matrice) bicolore (fond noir, bord blanc)
	( avec lissé, détection de bord et seuillage )

Sortie : nombre de régions trouvés (int)

Plat : Ragoût de veau et sauté de pomme de terre

Dessert : île flottante
