
#ifndef DEF_COMPTAGE_REGIONS // Si la constante n'a pas été définie le fichier n'a jamais été inclus

#define DEF_COMPTAGE_REGIONS // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../structure.h"

#define SEUIL_PROXIMITE 1


#endif

