#ifndef __DEF_STRUCTURES_STRUCTURE_ // Si la constante n'a pas été définie le fichier n'a jamais été inclus

#define __DEF_STRUCTURES_STRUCTURE_ // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus





/* struct Element */
/* { */
/*     int nombre; // objet voulu */
/*     Element *suivant; */
/* }; */
/* typedef struct Element Element; */


/* struct Liste */
/* { */
/*     Element *premier; */
/* }; */
/* typedef struct Liste Liste; */


enum State
{
    UNKNOWN, EXPLORED
};
typedef enum State State;


struct Pixel
{
    int r,g,b; // unsigned char ?
    State state; // état du pixel (croissance de région) initialiser à UNKNOWN
    int color;
};
typedef struct Pixel Pix;

struct Image
{
    unsigned long x,y,n,t;
    Pix** array; // tableau de tableau d'integer
};
typedef struct Image Image;


struct Point
{
    int x,y;
};
typedef struct Point Point;


struct Region
{
    int volume; // nombre de pixels de la région
    Point gravity_centrer; // centre géométrique de la région
    Point* contenu; // ensemble des coordonnées des points de la région
};
typedef struct Region Region;

#endif
