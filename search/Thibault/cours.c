#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CONSTANTE 3




void affiche(int tableau[], int taille)
{
	int i;
	for (i = 0 ; i < taille ; i++)
	{
		printf("%d\n", tableau[i]);
	}
}



int main(int argc, char *argv[])
{
	int age = 20;

	printf("Aire = %f\n !", 10);

	for (i = 0 ; i < 4 ; i++)
	{

	}

	int tableau[4];

	int tableau[4] = {10, 23}; // [10, 23, 0, 0]

	char chaine[] = "Roger";

	// string.h

	strlen(chaine);

	strcpy(copie, chaine); // On copie "chaine" dans "copie"

	strcat(chaine1, chaine2); // On concatène chaine2 dans chaine1

	if (strcmp(chaine1, chaine2) == 0) // Si chaînes identiques
	{
		printf("Les chaines sont identiques\n");
	}

	*suiteChaine = NULL;

	suiteChaine = strchr(chaine, 'g'); // ger

	suiteChaine = strpbrk("Texte de test", "xdp"); // xte de test

	suiteChaine = strstr("Texte de test", "de"); // de test

	// native

	sprintf(chaine, "Tu as %d ans !", 15); // chaine = Tu as 15 ans !



	return 0;
}


// structures

typedef struct Coordonnees Coordonnees;
struct NomDeVotreStructure
{
	int x;
	double z;
    char prenom[100];
};

NomDeVotreStructure variable = {0, 0}; // Création d'une variable de type NomDeVotreStructure

variable.x = 10;
variable.z = 20.2;

// enumérations

typedef enum Volume Volume;
enum Volume
{
    FAIBLE = 10, MOYEN = 50, FORT = 100
    // ou FAIBLE, MOYEN, FORT
};


// allocation dynamique

    int* memoireAllouee = NULL;
    memoireAllouee = malloc(sizeof(int)); // Allocation de la mémoire
    if (memoireAllouee == NULL)
    {
        exit(0);
    }

    // Utilisation de la mémoire
    printf("Quel age avez-vous ? ");
    scanf("%d", memoireAllouee);
    printf("Vous avez %d ans\n", *memoireAllouee);
    free(memoireAllouee); // Libération de mémoire


// créé un tableau dynamique
void* array(int elements, int taille)
{
	tab = malloc(elements * taille); // On alloue de la mémoire pour le tableau
	if (tab == NULL) // On vérifie si l'allocation a marché ou non
	{
		exit(0); // On arrête tout
	}
	return tab;
}

// supprime un tableau dynamique
void del_array(int tab[])
{
	free(tab);
}

